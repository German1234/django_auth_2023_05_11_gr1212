from django.urls import path, include
from users import views
from django.contrib.auth.views import PasswordChangeView, PasswordChangeDoneView, PasswordResetView

app_name = "users"
urlpatterns = [
    path("dashboard/", views.dashboard, name="dashboard"),
    path("accounts/", include("django.contrib.auth.urls")),
    path("change_password/", PasswordChangeView.as_view(
    template_name="registration/change_form.html",
    success_url="done/"), name="change"
    ),
    path("change_password/done/", PasswordChangeDoneView.as_view(template_name="registration/change_done.html"), name="change_done"),
    path("reset_password/", PasswordResetView.as_view(
        template_name = "registration/reset_password_form.html",
        success_url = "done/"), name="reset")
]